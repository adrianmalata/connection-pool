package database;

import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionWithDatabase {
    private final String USER_NAME;
    private final String PASSWORD;
    private final String URL;
    private java.sql.Connection connection;

    public ConnectionWithDatabase() {
        this.USER_NAME = "adrianmalata";
        this.PASSWORD = "somepassword";
        String DATABASE = "connectionpool_db";
        this.URL = String.format("jdbc:postgresql://localhost:5432/%s", DATABASE);
    }

    public void makeConnection() {
        try {
            connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        }
    }

    public java.sql.Connection getConnection() {
        return connection;
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        }
    }
}
