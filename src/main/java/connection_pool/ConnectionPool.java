package connection_pool;

import database.ConnectionWithDatabase;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private final int MAX_CONNECTIONS;
    private final List<ConnectionWithDatabase> FREE_CONNECTIONS;
    private final List<ConnectionWithDatabase> CONNECTION_POOL;
    private final ScheduledExecutorService SCHEDULER;
    private final Lock LOCK;

    public ConnectionPool() {
        this.MAX_CONNECTIONS = 100;
        this.FREE_CONNECTIONS = new ArrayList<>();
        this.CONNECTION_POOL = setNewList();
        this.LOCK = new ReentrantLock();
        this.SCHEDULER = Executors.newScheduledThreadPool(1);
        SCHEDULER.scheduleAtFixedRate(this::cleanupInactiveConnections, 1L, 1L, TimeUnit.MINUTES);
    }

    private void cleanupInactiveConnections() {
        LOCK.lock();
        try {
            Iterator<ConnectionWithDatabase> iterator = CONNECTION_POOL.iterator();
            while (iterator.hasNext()) {
                ConnectionWithDatabase connection = iterator.next();
                if (!FREE_CONNECTIONS.contains(connection) && !(isConnectionValid(connection))) {
                    iterator.remove();
                    connection.closeConnection();
                }
            }
        } finally {
            LOCK.unlock();
        }
    }

    private boolean isConnectionValid(ConnectionWithDatabase connection) {
        try {
            return connection.getConnection().isValid(1);
        } catch (SQLException e) {
            System.err.println("isValidConnection: The value supplied for timeout is less than 0");
            return false;
        }
    }

    private List<ConnectionWithDatabase> setNewList() {
        List<ConnectionWithDatabase> newList = new ArrayList<>();
        for (int x=0; x < 10; x++) {
            ConnectionWithDatabase connection = new ConnectionWithDatabase();
            newList.add(connection);
            FREE_CONNECTIONS.add(connection);
        }
        return newList;
    }

    public ConnectionWithDatabase getOrCreateConnection() {
        LOCK.lock();
        try {
            ConnectionWithDatabase connection;
            if (!FREE_CONNECTIONS.isEmpty()) {
                connection = getFreeConnection();
            } else {
                connection = createConnection();
            }
            return connection;
        } catch (Exception exception) {
            System.err.println(exception.getMessage());
            return null;
        } finally {
            LOCK.unlock();
        }
    }

    private ConnectionWithDatabase getFreeConnection() {
        ConnectionWithDatabase connection = null;
        while (connection == null) {
            connection = searchForFreeConnection();
            connection = returnConnectionIfValid(connection);
        }
        return connection;
    }

    private ConnectionWithDatabase searchForFreeConnection() {
        if (!FREE_CONNECTIONS.isEmpty()) {
            return FREE_CONNECTIONS.remove(0);
        } else {
            System.err.println("getFreeConnection: No free connections available");
            return null;
        }
    }

    private ConnectionWithDatabase returnConnectionIfValid(ConnectionWithDatabase connection) {
        return (isConnectionValid(connection)) ? connection : null;
    }

    private ConnectionWithDatabase createConnection() {
        if (CONNECTION_POOL.size() < MAX_CONNECTIONS) {
            ConnectionWithDatabase connection = new ConnectionWithDatabase();
            connection.makeConnection();
            CONNECTION_POOL.add(connection);
            return connection;
        } else {
            System.err.println("CONNECTION_POOL: Sorry, you cannot add more connections");
            cleanConnectionPool();
            return null;
        }
    }

    private void cleanConnectionPool() {
        if (CONNECTION_POOL.size() >= MAX_CONNECTIONS) {
            CONNECTION_POOL.removeAll(FREE_CONNECTIONS);
            FREE_CONNECTIONS.clear();
        }
    }

    public void releaseConnection(ConnectionWithDatabase connection) {
        if (CONNECTION_POOL.size() >= MAX_CONNECTIONS) {
            CONNECTION_POOL.remove(connection);
            connection.closeConnection();
        } else {
            FREE_CONNECTIONS.add(connection);
        }
    }

    public List<ConnectionWithDatabase> getConnectionPool() {
        return CONNECTION_POOL;
    }

    public ScheduledExecutorService getScheduler() {
        return SCHEDULER;
    }
}

