package exercise;

import database.ConnectionWithDatabase;
import org.jooq.*;
import org.jooq.Record;
import org.jooq.impl.DSL;
import static org.jooq.impl.DSL.*;
import static org.jooq.impl.SQLDataType.INTEGER;


public class Exercise {
    private static final ConnectionWithDatabase CONNECTION_WITH_DATABASE_WITH_DATABASE = new ConnectionWithDatabase();
    private static final String TEST_TABLE_NAME = "test_table";
    private static final String RECORD_FIELD_NAME = "record";
    private static final Table<Record> TEST_TABLE = table(TEST_TABLE_NAME);
    private static final Field<Object> RECORD_FIELD = field(RECORD_FIELD_NAME);
    private static DSLContext jOOQ;

    private static void openConnection() {
        CONNECTION_WITH_DATABASE_WITH_DATABASE.makeConnection();
        jOOQ = DSL.using(CONNECTION_WITH_DATABASE_WITH_DATABASE.getConnection(), SQLDialect.POSTGRES);
    }

    private static void setupNewTable() {
        jOOQ.dropTableIfExists(TEST_TABLE).execute();
        jOOQ.createTable(TEST_TABLE_NAME).column(RECORD_FIELD_NAME, INTEGER).execute();
    }

    private static void insertNewValueIntoTable(int i) {
        jOOQ.insertInto(TEST_TABLE, RECORD_FIELD).values(i).execute();
    }

    private static void dropTable() {
        jOOQ.dropTable(TEST_TABLE).execute();
    }

    private static void exercise1() {
        openConnection();
        setupNewTable();
        CONNECTION_WITH_DATABASE_WITH_DATABASE.closeConnection();

        long start = System.currentTimeMillis();

        for(int i=0; i < 10000; i++) {
            openConnection();
            insertNewValueIntoTable(i);
            CONNECTION_WITH_DATABASE_WITH_DATABASE.closeConnection();
        }

        long stop = System.currentTimeMillis();
        long difference = (stop - start) / 1000;

        openConnection();
        dropTable();
        CONNECTION_WITH_DATABASE_WITH_DATABASE.closeConnection();

        System.out.printf("\nRepeated connecting took %d second(s)\n", difference);
    }

    private static void exercise2() {
        openConnection();
        setupNewTable();

        long start = System.currentTimeMillis();

        for(int i=0; i < 10000; i++) {
            insertNewValueIntoTable(i);
        }

        long stop = System.currentTimeMillis();
        long difference = (stop - start) / 1000;

        dropTable();
        CONNECTION_WITH_DATABASE_WITH_DATABASE.closeConnection();

        System.out.printf("\nOne connection took %d second(s)\n", difference);
    }

    public static void main(String[] args) {
        exercise1();
        exercise2();
    }
}
