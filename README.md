## Connection Pool

***

### Link

https://youtu.be/_FOd4BaDEfg

### Name

Connection pool library.

### Description

* A mechanism that allows to reuse database connections.
* At the start of the application, there are created 10 connections, which are constantly available.
* If necessary, more are created, disconnected and destroyed when the load decreases.
* The pool can hold a maximum of 100 connections to the base, when an attempt is made to get another one, a message is returned that this is currently not possible.

### Guide

1. Check the settings of database connection

### Libraries

* JUnit Jupiter 5.10.1
* JDBC 42.6.0
* JOOQ 3.18.7

***
