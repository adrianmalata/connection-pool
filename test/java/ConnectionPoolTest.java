import connection_pool.ConnectionPool;
import database.ConnectionWithDatabase;
import org.jooq.*;
import org.jooq.Record;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.Test;

import java.util.concurrent.*;

import static org.jooq.impl.DSL.*;
import static org.jooq.impl.SQLDataType.INTEGER;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConnectionPoolTest {
    private final ConnectionPool CONNECTION_POOL = new ConnectionPool();
    private final Table<Record> TEST_TABLE = table("test_table");
    private final Field<Object> RECORD_FIELD = field("record");
    private final ConnectionWithDatabase CONNECTION = new ConnectionWithDatabase();
    private DSLContext jOOQ;

    @Test
    public void stressTest() {
        int records = 10000;
        setupNewTable();
        CountDownLatch countDownLatch = new CountDownLatch(records);
        ExecutorService executorService =
                new ThreadPoolExecutor(
                        100,
                        100,
                        0L,
                        TimeUnit.MILLISECONDS,
                        new LinkedBlockingDeque<>()
                );
        try {
            for (int x=0; x < records; x++) {
                executorService.submit(
                        () -> {
                            ConnectionWithDatabase localConnection = null;
                            while (localConnection == null) {
                                localConnection = CONNECTION_POOL.getOrCreateConnection();
                                try {
                                    if (localConnection != null) {
                                        DSLContext localJOOQ = DSL.using(localConnection.getConnection(), SQLDialect.POSTGRES);
                                        localJOOQ.insertInto(TEST_TABLE, RECORD_FIELD).values(1).execute();
                                        System.out.println("Connection pool size: " + CONNECTION_POOL.getConnectionPool().size());
                                        System.out.println("countDownLatch: " + countDownLatch.getCount());
                                        CONNECTION_POOL.releaseConnection(localConnection);
                                        countDownLatch.countDown();
                                    } else {
                                        System.err.println("executorService: No available connection, waiting and retrying...");
                                        Thread.sleep(100);
                                    }
                                } catch (Exception exception) {
                                    System.err.println(exception.getMessage());
                                }
                            }
                        }
                );
            }
            countDownLatch.await();
            executorService.shutdown();
            CONNECTION_POOL.getScheduler().shutdown();
            if (executorService.awaitTermination(15, TimeUnit.SECONDS)) {
                int result = getResult();
                assertEquals(records, result);
            }
        } catch (InterruptedException interruptedException) {
            System.err.println("countDownLatch: the current thread is interrupted while waiting");
        }
    }

    private void setupNewTable() {
        openConnection();
        jOOQ.dropTableIfExists(TEST_TABLE).execute();
        jOOQ.createTable(TEST_TABLE).column(recordFieldName, INTEGER).execute();
        CONNECTION.closeConnection();
    }

    private int getResult() {
        openConnection();
        int result = jOOQ.select(asterisk()).from(TEST_TABLE).execute();
        CONNECTION.closeConnection();
        return result;
    }

    private void openConnection() {
        CONNECTION.makeConnection();
        jOOQ = DSL.using(CONNECTION.getConnection(), SQLDialect.POSTGRES);
    }
}
